Description
-----------
The module integrates the Robokassa payment gateway into the main "Payer" module

Requirements
-----------
Drupal 7.x

Installation
-----------
1. Copy the whole payer directory to the sites directory /all/modules/payer/methods

2. Log on to the system as an administrator. Enable the module on the
"Administration" -> "Modules". Module "Payer Robokassa".

3. (Optional) Edit the module settings in the section
"Administration" -> "Configuration" -> "Payer" -> "Settings"